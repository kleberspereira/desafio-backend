const fs = require('fs');
const { parse } = require('path/win32');
//Abre o arquivo de banco de dados fake
let rawdata = fs.readFileSync('fakeData.json');
//converte parao formato desejado
let fakeData = JSON.parse(rawdata);

class Controller {
    // Pega todos os registros
    async getAll() {
        // retorna todos os dados
        return new Promise((resolve, _) => resolve(fakeData));
    }

    // Pega um unico registro
    async get(id) {
        return new Promise((resolve, reject) => {
            // pega o registro pelo id
            let data = fakeData.find((data) => data.id === parseInt(id));
            if (data) {
                // retorna o registro se achar
                resolve(data);
            } else {
                // retonar mensagem se nao encontrar
                reject(`Nenhum registro com id ${id} encontrado`);
            }
        });
    }

    // Cria um registro
    async create(data) {
        return new Promise((resolve, _) => {
            // cria o registro com um random id
            let newData = {
                id: Math.floor(4 + Math.random() * 10),
                ...data,
            };
            //Abre o arquivo de banco de dados fake
            let rwdata = fs.readFileSync('fakeData.json');
            //converte parao formato desejado
            let fkData = JSON.parse(rawdata);
            //Adiciona o novo no
            fkData.push(newData);
            //escreve no arquivo com o novo no e atualiza os dados
            fakeData = fs.writeFileSync("fakeData.json", JSON.stringify(fkData));
            // retorna o registro criando
            resolve(newData);
        });
    }

    // Atualiza um registro
    async update(id,data) {
        return new Promise((resolve, reject) => {
            // pega o registro pelo id
            let dataReg = fakeData.find((dataReg) => dataReg.id === parseInt(id));
            // se nao encontrar o resgitro retorna erro
            if (!dataReg) {
                reject(`Nenhum registro com id ${id} encontrado`);
            }
            //converte os dados para json
            let json = JSON.parse(data);          
            //atualiza o registro com os novos dados
            dataReg["document"] = json.document;
            dataReg["amont"] = json.amont;
            //escreve no arquivo fazendo a alteracao permanente
            fs.writeFileSync("fakeData.json", JSON.stringify(fakeData));
            // retorna o registro atualizado
            resolve(dataReg);
        });
    }

    // deletando um registro
    async delete(id) {
        return new Promise((resolve, reject) => {
            // pega o registro pelo id
            let dataReg = fakeData.find((dataReg) => dataReg.id === parseInt(id));
            // se nao encontrar retorna erro
            if (!dataReg) {
                reject(`Nenhum registro com id ${id} encontrado`);
            }
            //Deleta o registro
            for (let [i, reg] of fakeData.entries()) {
               if (reg.id == dataReg["id"]) {
                fakeData.splice(i, 1);
               }
            }            
            //escreve no arquivo fazendo a alteracao permanente
            fakeData = fs.writeFileSync("fakeData.json", JSON.stringify(fakeData));
            // retorna mensagem se deletado
            resolve(`Registro deletado com sucesso`);
        });
    }

   // Tranfere um registro
   async transfer(idOrigem,idDestino,valor) {
    return new Promise((resolve, reject) => {
            // pega o registro de origem
            let origem = fakeData.find((origem) => origem.id === parseInt(idOrigem));
            // se nao encontrar o resgitro retorna erro
            if (!origem) {
                reject(`Nenhum registro de origem com id ${id} encontrado`);
            }
            // pega o registro de destino
            let destino = fakeData.find((destino) => destino.id === parseInt(idDestino));
            // se nao encontrar o resgitro retorna erro
            if (!destino) {
                reject(`Nenhum registro de destino com id ${id} encontrado`);
            }            

            //realiza transferencia
            if (origem["amont"] >= parseInt(valor)){
                origem["amont"] -= parseInt(valor);
                destino["amont"] += parseInt(valor);    
                //escreve no arquivo fazendo a alteracao permanente
                fs.writeFileSync("fakeData.json", JSON.stringify(fakeData));
                // retorna a mesnsagem
                resolve(`Transferencia realizada com sucesso`);  
            }else{
                reject(`Saldo insuficiente na origem`);
            }
        });
    }    
}
module.exports = Controller;