// app.js
// Kleber S. Pereira 20/12/2021
// run: 
// nodemon app.js 
const http = require("http");
const controller = require("./controller");
const { getReqData } = require("./utils");

const PORT = process.env.PORT || 8080;

const server = http.createServer(async (req, res) => {
    
    // rota "/" : lista todos os registros
    // EX.: http://localhost:8080/
    if (req.url === "/" && req.method === "GET") {
        // pega os registros.
        const fakeData = await new controller().getAll();
        // define o status e tipo de conteudo
        res.writeHead(200, { "Content-Type": "application/json" });
        // envia os registros
        res.end(JSON.stringify(fakeData));
    }

    // rota "/:id" : Lista um unico registro
    // EX.: http://localhost:8080/2
    else if (req.url.match(/\/([0-9]+)/) && req.method === "GET") {
        try {
            // pega o id pela url
            const id = req.url.split("/")[1];
            // pega o registro pelo id
            const fakeData = await new controller().get(id);
            // define o status e tipo de conteudo
            res.writeHead(200, { "Content-Type": "application/json" });
            // envia os registros
            res.end(JSON.stringify(fakeData));
        } catch (error) {
            // define o status e tipo de conteudo
            res.writeHead(404, { "Content-Type": "application/json" });
            // emvia o erro
            res.end(JSON.stringify({ msg: error }));
        }
    }

    /*  rota "/" : Cria um novo registro
        EX.: http://localhost:8080/
        payload:
        {
            "document":"555.555.5555/55",
            "amont" : 500.00
        }
    */
    else if (req.url === "/" && req.method === "POST") {
        // pega os dados dos prarametros
        let data = await getReqData(req);
        // cria o registro
        let fakeData = await new controller().create(JSON.parse(data));
        // define o status e tipo de conteudo
        res.writeHead(200, { "Content-Type": "application/json" });
        //retorna o registro criado
        res.end(JSON.stringify(fakeData));
    }

    /*  rota "/:id" : Atualiza um registro
        EX.: http://localhost:8080/1
        PAYLOAD:
        {
		    "document": "111.111.1111-22",
		    "amont": 999
        }
    */    
    else if (req.url.match(/\/([0-9]+)/) && req.method === "PUT") {
        try {
            // pega o id pela url
            const id = req.url.split("/")[1];
            // pega os dados dos prarametros
            let data = await getReqData(req);
            // atualiza o registro
            let updateData = await new controller().update(id,data);
            // define o status e tipo de conteudo
            res.writeHead(200, { "Content-Type": "application/json" });
            //retorna o registro criado
            res.end(JSON.stringify(updateData));
        } catch (error) {
            // define o status e tipo de conteudo
            res.writeHead(404, { "Content-Type": "application/json" });
            // envia o erro
            res.end(JSON.stringify({ message: error }));
        }
    }

    // rota "/:id" : deleta um registro
    // EX.: http://localhost:8080/10
    else if (req.url.match(/\/([0-9]+)/) && req.method === "DELETE") {
        try {
            // pega o id pela url
            const id = req.url.split("/")[1];
            // deleta um registro
            let msg = await new controller().delete(id);
            // define o status e tipo de conteudo
            res.writeHead(200, { "Content-Type": "application/json" });
            // emvia a mensagem
            res.end(JSON.stringify({ msg }));
        } catch (error) {
            // define o status e tipo de conteudo
            res.writeHead(404, { "Content-Type": "application/json" });
            // envia o erro
            res.end(JSON.stringify({ msg: error }));
        }
    }

    // rota "/transferir/:id/:id/:valor" : transfere de uma origem para um destino
    //http://localhost:8080/t/1/2/50
    else if (req.url.match(/\/t\//) && req.method === "POST") {
        try {
            // pega o id origem pela url
            const idOrigem = req.url.split("/")[2];
            // pega o id destino pela url
            const idDestino = req.url.split("/")[3];
            // pega o valor pela url
            const valor = req.url.split("/")[4];
            // transfere o valor
            let updateData = await new controller().transfer(idOrigem,idDestino,valor);
            // define o status e tipo de conteudo
            res.writeHead(200, { "Content-Type": "application/json" });
            //retorna o registro criado
            res.end(JSON.stringify(updateData));
        } catch (error) {
            // define o status e tipo de conteudo
            res.writeHead(404, { "Content-Type": "application/json" });
            // envia o erro
            res.end(JSON.stringify({ message: error }));
        }
    }

    // Nao encontrou nenhuma rota
    else {
        res.writeHead(404, { "Content-Type": "application/json" });
        res.end(JSON.stringify({ message: "Rota nao encontrada" }));
    }
});

server.listen(PORT, () => {
    console.log(`Servidor iniciado na porta: ${PORT}`);
});