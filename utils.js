//utils.js
function getReqData(req) {
    return new Promise((resolve, reject) => {
        try {
            let data = "";
            // recebendo os dados enviados do cliente
            req.on("data", (chunk) => {
                // adicionando a string
                data += chunk.toString();
            });
            // acabou
            req.on("end", () => {
                // enviando de volta
                resolve(data);
            });
        } catch (erro) {
            reject(erro);
        }
    });
}
module.exports = { getReqData };